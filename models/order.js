const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	userId: String,
	purchasedOn:{
		type: Date,
		default: new Date()
	},
	products:[
		{
			productId:{
				type: String,
				required: [true, 'Product ID is required']
			},
			quantity:{
				type: Number,
				required: [true, 'Quantity is required']
			}
		}
	],
	totalAmount: Number,
})

module.exports = mongoose.model("Order", orderSchema)




