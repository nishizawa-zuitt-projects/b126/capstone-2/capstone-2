const mongoose = require("mongoose")


const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, 'Product name is required']
	},
	productCategory:{
		type: String,
		required: [true,'product category is required']
	},
	description:{
		type: String,
		required: [true,'Description is required']
	},
	price:{
		type: Number,
		required: [true,'Price is required']
	},
	releasedOn:{
		type: Date,
		default: new Date()
	},
	isAvailable:{
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model("Product", productSchema)