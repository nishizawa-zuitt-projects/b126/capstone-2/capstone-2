const Product = require("../models/product")


//For posting a new product
module.exports.postProduct = (body) => {
	let newProduct = new Product({
		name: body.name,
		productCategory: body.productCategory,
		description: body.description,
		price: body.price
	})

	return newProduct.save().then((product,error) =>{
		if(error){
			return false; // course was NOT saved
		}else {
			return true; // course was successfully saved
		}
	})
}

//For retrieving all the available products
module.exports.getAllProducts =()=>{
	return Product.find({isAvailable:true}).then(result =>{
		return result
	})
}

//For getting a specific product
module.exports.getProduct = (params)=>{
	// findById is a Mongoose operation that just finds a document by its ID
	return Product.findById(params.productId).then(result =>{
		return result
	})
}


//For updating a specific product
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name:body.name,
		productCategory:body.productCategory,
		description: body.description,
		price: body.price
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product,err) =>{
		if(err){
			return false
		}else{
			return true
		}
	})
}


//For archiving/deleting a product
module.exports.archiveProduct = (params) =>{
	let archivedProduct = {
		isAvailable: false
	}

	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}











