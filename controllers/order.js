const User = require("../models/user")
const Product = require("../models/product")
const Order = require("../models/order")
const bcrypt = require("bcrypt")
const auth = require("../auth")



//For Non-admin user to place an order
module.exports.placeOrder = (data) =>{

	let newOrder = new Order({
		userId:data.userId,
		products: data.products,
		totalAmount: data.totalAmount
	})

	return newOrder.save().then((order,error) =>{
		if(error){
			return false;
		}else {
			return true;
		}
	})

}

//For a user to get their order history
module.exports.userOrderHis =(myHist)=>{
	return Order.find({userId:myHist.id}).then(result =>{
		if(result===null){
			return res.send("No order yet")
		}else{
			return result
		}
	})
}

//For Admin user to retrieve all orders
module.exports.getAllOrders =()=>{
	return Order.find({}).then(result =>{
		return result
	})
}


