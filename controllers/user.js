const User = require("../models/user")
const Product = require("../models/product")
const bcrypt = require("bcrypt")
const auth = require("../auth")

//For checking duplicate email before registering a user
module.exports.checkEmail = (body) =>{
	return User.find({email: body.email}).then(result =>{
		if(result.length>0){
			return true
		}else{
			return false
		}
	})	
}

//If no duplicate, the user can proceed to register
module.exports.registerUser = (body) =>{
	let newUser = new User({
		firstName:body.firstName,
		lastName:body.lastName,
		email:body.email,
		mobileNo:body.mobileNo,
		password:bcrypt.hashSync(body.password, 10),
		address:body.address
	})

	return newUser.save().then((user,error) =>{
		if(error){
			return false; // user was NOT saved
		}else {
			return true; // user was successfully saved
		}
	})
}

//For user login
module.exports.loginUser = (body) =>{
	
	return User.findOne({email: body.email}).then(result=>{
		if(result === null){
			return false

		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				
				return{access: auth.createAccessToken(result.toObject())}
			}else{
				return false
			}
		}
	})
}

//For a users to get their own profile details when logged in
module.exports.getProfile =(userData)=>{
	return User.findById(userData.id).then(result =>{
		result.password = undefined
		return result
	})
}



