const express = require("express");
const router = express.Router()
const orderController = require("../controllers/order")
const auth = require("../auth")


//For Non-admin user to place an order
router.post("/purchase", auth.verify, (req,res)=>{
	let data = {
			userId: auth.decode(req.headers.authorization).id,
			products: req.body.products,
			totalAmount: req.body.totalAmount
		}

	if(auth.decode(req.headers.authorization).isAdmin){
		res.send({auth: "failed"})
	}else{
		orderController.placeOrder(data).then(resultFromController => res.send(resultFromController))
	}
})


//Route for a user to get their order history
router.get("/myOrders", auth.verify, (req, res) =>{
	const myHist = auth.decode(req.headers.authorization)
	orderController.userOrderHis(myHist).then(resultFromController => res.send(resultFromController))
})


//for admin to retrieve all orders
router.get("/", auth.verify, (req, res)=> {
	console.log(auth.decode(req.headers.authorization))
	
	if (auth.decode(req.headers.authorization).isAdmin){
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})


module.exports = router