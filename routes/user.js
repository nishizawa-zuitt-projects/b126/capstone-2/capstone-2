const express = require("express");
const router = express.Router()
const userController = require("../controllers/user")
const auth = require("../auth")


//Routes for checking a duplicate email before registering a user
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})


//Route for registration
router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


//Route for login
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


//Route for a user to get their details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
})




module.exports = router